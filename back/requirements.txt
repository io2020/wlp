flask==1.1.2
sqlalchemy==1.3.16
flask-sqlalchemy==2.4.1
flask-cors==3.0.8
xlrd==1.2.0
psycopg2-binary==2.8.5
gevent==20.5.0
gunicorn==20.0.4
flask-WeasyPrint==0.6