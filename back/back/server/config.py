import os
from typing import cast

DB_HOST = os.getenv("DB_HOST")
DB_PORT = int(cast(str, os.getenv("DB_PORT")))
DB_USER = os.getenv("DB_USER")
DB_PASS = os.getenv("DB_PASS")
DB_DATABASE = os.getenv("DB_DATABASE")
