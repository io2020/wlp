from flask import Blueprint, jsonify, make_response, render_template
import os

from .queries import find_chart_data
from .responses import no_med_response, create_med_details, search_results
from .utils import verify_ean, get_med_details_components
from .pdf_generator import create_pdf

bp = Blueprint("meds_bp", __name__, template_folder='pdf_supplies')

@bp.route('/<ean>')
def performSearch(ean : str):
    ean = verify_ean(ean)
    if ean is False:
        return no_med_response, 200

    cur_med_info, active_substances, price_by_payment_level = get_med_details_components(ean)
    if cur_med_info is False:
        return no_med_response, 200

    med_details = create_med_details(cur_med_info, active_substances,
                                     price_by_payment_level)
    chart_data = find_chart_data(active_substances)

    return jsonify(search_results(med_details, chart_data)), 200

@bp.route('/<ean>.pdf')
def serve_pdf(ean : str):
    ean = verify_ean(ean)
    if ean is False:
        return no_med_response, 404

    cur_med_info, active_substances, price_by_payment_level = get_med_details_components(ean)
    if cur_med_info is False:
        return no_med_response, 404

    price_by_payment_level = sorted(price_by_payment_level.items())

    med_details = create_med_details(cur_med_info, active_substances,
                                     price_by_payment_level)

    pdf = create_pdf(med_details, f'{ean}.pdf')
    
    return pdf