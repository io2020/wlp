from flask import render_template
from flask_weasyprint import HTML, CSS, render_pdf
import os

pdf_template = 'pdf_template.html'
css = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'pdf_supplies/pdf.css')

def join_substances(active_substances):
    return ", ".join(active_substances)

def create_pdf(med_details, filename):
    html = render_template(pdf_template, details=med_details, join_substances=join_substances)

    return render_pdf(HTML(string=html), stylesheets=[CSS(filename=css)],
                      download_filename=filename, automatic_download=False)