from .queries import find_med_info, find_med_substances, find_latest_prices_by_payment_level

def verify_ean(ean : str):
    if not ean.isdigit():
        return False

    ean = ean.lstrip("0")
    if ean == "":
        return False

    return ean

def get_med_details_components(ean : str):
    cur_med_info = find_med_info(ean)

    if cur_med_info is None:
        return False, False, False

    active_substances = find_med_substances(cur_med_info.ean)
    price_by_payment_level = find_latest_prices_by_payment_level(cur_med_info.id)

    return cur_med_info, active_substances, price_by_payment_level