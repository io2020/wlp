from sqlalchemy import Column, Integer, Text, Date, ForeignKey, UniqueConstraint
from back.server import db


class MedSubstance(db.Model):
    __tablename__ = 'med_substance'

    id = Column(Integer, primary_key=True)
    ean = Column(Text, nullable = False)
    active_substance = Column(Text, nullable = False)
    def __repr__(self):
        return f"<MedSubstance(ean='{self.ean}', active_substance='{self.active_substance}')>"

    UniqueConstraint(ean, active_substance)


class MedInfo(db.Model):
    __tablename__ = 'med_info'

    id = Column(Integer, primary_key=True)
    ean = Column(Text, nullable = False)
    name = Column(Text, nullable = False)
    dose = Column(Text, nullable = False)
    form = Column(Text, nullable = False)
    box_contents = Column(Text, nullable = False)
    def __repr__(self):
        return f"<MedInfo(ean='{self.ean}', name='{self.name}', dose='{self.dose}', "\
               f"form='{self.form}', box_contents='{self.box_contents}')>"

    UniqueConstraint(ean)


class MedRefund(db.Model):
    __tablename__ = 'med_refund'

    id = Column(Integer, primary_key=True)
    med_info_id = Column(Integer, ForeignKey('med_info.id'))
    payment_level = Column(Text, nullable = False)
    def __repr__(self):
        return f"<MedRefund(med_info_id='{self.med_info_id}', payment_level='{elf.payment_level}')>"

    UniqueConstraint(med_info_id, payment_level)


class MedPrice(db.Model):
    __tablename__ = 'med_price'

    id = Column(Integer, primary_key=True)
    med_refund_id = Column(Integer, ForeignKey('med_refund.id'))
    pub_date = Column(Date, nullable = False)
    price = Column(Text, nullable = False)
    def __repr__(self):
        return f"<MedPrice(med_refund_id='{self.med_refund_id}',"\
               f" pub_date='{self.pub_date}', price='{self.price}')>"

    UniqueConstraint(med_refund_id, pub_date)


MODELS = [MedInfo, MedSubstance, MedPrice, MedRefund]
