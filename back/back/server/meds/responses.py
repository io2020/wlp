no_med_response = {
                    "error": "med does not exist"
                }

def create_med_details(med_info, active_substances, price_by_payment_level):
    return {
        "ean": med_info.ean,
        "name": med_info.name,
        "active_substances": active_substances,
        "form": med_info.form,
        "dose": med_info.dose,
        "box_contents": med_info.box_contents,
        "price_by_payment_level": price_by_payment_level
    }

def search_results(med_details, chart_data):
    return {
        "med": med_details,
        "chart": chart_data
    }