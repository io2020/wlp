from sqlalchemy import func
from flask_sqlalchemy import SQLAlchemy
from copy import deepcopy

from back.server import db
from .models import MedInfo, MedSubstance, MedRefund, MedPrice

from sqlalchemy.dialects import postgresql


def find_med_info(ean: str):
    return MedInfo.query.filter(MedInfo.ean == str(ean)).one_or_none()


def find_med_substances(ean: str):
    substances = MedSubstance.query \
                            .with_entities(MedSubstance.active_substance) \
                            .filter(MedSubstance.ean == ean) \
                            .all()

    return [subst.active_substance for subst in substances]


def find_latest_prices_by_payment_level(med_info_id: str):
    refunds = MedRefund.query \
                    .with_entities(MedRefund.id, MedRefund.payment_level) \
                    .filter(MedRefund.med_info_id == med_info_id) \
                    .subquery()

    latest_dates = MedPrice.query \
                        .with_entities(
                            MedPrice.med_refund_id.label('id'),
                            refunds.c.payment_level,
                            func.max(MedPrice.pub_date).label('max_date')
                        ) \
                        .filter(MedPrice.med_refund_id == refunds.c.id) \
                        .group_by(MedPrice.med_refund_id, refunds.c.payment_level) \
                        .subquery()

    price_by_payment_level = MedPrice.query \
                                .with_entities(latest_dates.c.payment_level, MedPrice.price) \
                                .filter(latest_dates.c.id == MedPrice.med_refund_id) \
                                .filter(latest_dates.c.max_date == MedPrice.pub_date) \
                                .all()

    return dict(price_by_payment_level)


def get_all_meds_info(similar_meds):
    all_meds_info = []

    for med in similar_meds:
        med_info_for_chart_base = {
            "ean": med.ean,
            "name": med.name,
            "form": med.form,
            "dose": med.dose
        }
        payment_levels = MedRefund.query \
                                .with_entities(MedRefund.id, MedRefund.payment_level) \
                                .filter(MedRefund.med_info_id == med.id) \
                                .all()

        for payment_level in payment_levels:
            single_entry = deepcopy(med_info_for_chart_base)
            single_entry["payment_level"] = payment_level.payment_level

            price_by_time = MedPrice.query \
                                    .with_entities(MedPrice.pub_date, MedPrice.price) \
                                    .filter(MedPrice.med_refund_id == payment_level.id) \
                                    .order_by(MedPrice.pub_date) \
                                    .all()

            price_by_time = [(date.strftime('%Y-%m'), price) for (date, price) in price_by_time]
            price_by_time = dict(price_by_time)

            single_entry["price_by_time"] = price_by_time

            all_meds_info.append(single_entry)

    return all_meds_info


def find_chart_data(active_substances):
    similar_med_eans = MedSubstance.query \
                                .with_entities(MedSubstance.ean) \
                                .distinct(MedSubstance.ean) \
                                .filter(MedSubstance.active_substance.in_(active_substances)) \
                                .subquery()

    similar_meds = MedInfo.query \
                        .with_entities(MedInfo.id, MedInfo.name, MedInfo.ean, MedInfo.form, MedInfo.dose) \
                        .filter(MedInfo.ean.in_(similar_med_eans)) \
                        .all()

    return get_all_meds_info(similar_meds)
