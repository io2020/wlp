import itertools

from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

def init_app():
    from . import config

    app = Flask(__name__)
    CORS(app, supports_credentials=True)
    app.url_map.strict_slashes = False

    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    app.config['SQLALCHEMY_DATABASE_URI'] = (
        f"postgresql://{config.DB_USER}:{config.DB_PASS}"
        f"@{config.DB_HOST}:{config.DB_PORT}/{config.DB_DATABASE}"
    )


    db = SQLAlchemy(app)

    return app, db

def init_others(db):
    from back.server import meds, healthz

    app.register_blueprint(healthz.bp, url_prefix="/")
    app.register_blueprint(meds.bp, url_prefix="/meds")

    db_models = meds.MODELS
    db.create_all()

    return db_models

app, db = init_app()
db_models = init_others(db)
