from flask import Blueprint, jsonify

bp = Blueprint("healthz_bp", __name__)

@bp.route("/")
def healthz():
    return jsonify(ok=True)
