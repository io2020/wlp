import os, glob
from sqlalchemy import *
from sqlalchemy.orm import sessionmaker
import xlrd

from back.server import db
from back.server.meds.models import MedInfo, MedSubstance, MedPrice, MedRefund

db.engine.execute("BEGIN")
db.drop_all()
db.create_all()
db.engine.execute("COMMIT")
db.engine.execute("SET DateStyle to 'YMD'")

# indexes of rows (starting from 0) from which data begins in respecting sheets
A1_DATA_BEGIN_R = 3
A2_DATA_BEGIN_R = 2
A3_DATA_BEGIN_R = 2

# indexes of columns (starting from 0) in which certain data is placed
ACT_SUBS_C = 1
NAME_DOSE_C = 2
CONTENTS_C = 3
EAN_C = 4
REFUND_C = 12
PAYMENT_LVL_C = 14
PRICE_C = 15

dirname = os.path.dirname(__file__)
path = os.path.join(dirname, 'xml-files')
file_format = 'xlsx'


Session = sessionmaker()
Session.configure(bind=db.engine)
session = Session()

def parse_active_substances(activeSubstances: str):
    return activeSubstances.split(" + ")

def add_substances(sheet, BEGIN_ROW):
    for i in range (BEGIN_ROW, sheet.nrows):
        ean = sheet.cell(i, EAN_C).value.lstrip("0")
        active_substance = sheet.cell(i, ACT_SUBS_C).value
        if ean == "":
            continue
        if session.query(MedSubstance.ean)\
        .filter(MedSubstance.ean == ean)\
        .count() == 0:
            sub_list = parse_active_substances(active_substance)
            for sub in sub_list:
                session.add(MedSubstance(ean = ean, active_substance = sub))
    session.commit()

def add_info(sheet, BEGIN_ROW):
    for i in range (BEGIN_ROW, sheet.nrows):
        ean = sheet.cell(i, EAN_C).value.lstrip("0")
        name = sheet.cell(i, NAME_DOSE_C).value.split(", ")[0]
        if len(sheet.cell(i, NAME_DOSE_C).value.split(", ")) == 2:
            form = sheet.cell(i, NAME_DOSE_C).value.split(", ")[1]
            dose = 'Brak danych'
        if len(sheet.cell(i, NAME_DOSE_C).value.split(", ")) == 3:
            form = sheet.cell(i, NAME_DOSE_C).value.split(", ")[1]
            dose = sheet.cell(i, NAME_DOSE_C).value.split(", ")[2]
        if len(sheet.cell(i, NAME_DOSE_C).value.split(", ")) > 3:
            form = ', '.join(sheet.cell(i, NAME_DOSE_C).value.split(", ")[1 : -1])
            dose = sheet.cell(i, NAME_DOSE_C).value.split(", ")[-1]

        box_contents = sheet.cell(i, CONTENTS_C).value
        if session.query(MedInfo.ean)\
            .filter(MedInfo.ean == ean)\
            .count() == 0:
            session.add(MedInfo(ean = ean, name = name, dose = dose, form = form, box_contents = box_contents))
    session.commit()

def add_refund(sheet, BEGIN_ROW):
    for i in range (BEGIN_ROW, sheet.nrows):
        payment_level = sheet.cell(i, PAYMENT_LVL_C).value
        ean = sheet.cell(i, EAN_C).value.lstrip("0")
        med_info_id = session.query(MedInfo.id)\
            .filter(MedInfo.ean == ean).one()

        if session.query(MedRefund.id)\
            .filter(MedRefund.med_info_id == med_info_id, MedRefund.payment_level == payment_level)\
            .count() == 0:
            session.add(MedRefund(med_info_id = med_info_id, payment_level = payment_level))

    session.commit()

def add_price(sheet, BEGIN_ROW, date):
    for i in range (BEGIN_ROW, sheet.nrows):
        payment_level = sheet.cell(i, PAYMENT_LVL_C).value
        ean = sheet.cell(i, EAN_C).value.lstrip("0")
        price = sheet.cell(i, PRICE_C).value.replace(',', '.')

        med_info_id = med_info_id = session.query(MedInfo.id)\
            .filter(MedInfo.ean == ean).one()
        med_refund_id = session.query(MedRefund.id)\
            .filter(MedRefund.med_info_id == med_info_id, MedRefund.payment_level == payment_level).one()
        if session.query(MedPrice.id)\
            .filter(MedPrice.med_refund_id== med_refund_id, MedPrice.pub_date == date)\
            .count() == 0:
            session.add(MedPrice(med_refund_id = med_refund_id, pub_date = date, price = price))

    session.commit()

def add_sheet(sheet, BEGIN_ROW, date):
    add_substances(sheet, BEGIN_ROW)
    add_info(sheet, BEGIN_ROW)
    add_refund(sheet, BEGIN_ROW)
    add_price(sheet, BEGIN_ROW, date)

def add_excel_file(filename : str):
    basename = os.path.basename(filename) # cur directory
    print(f'Adding: {basename}...')

    date = os.path.splitext(basename)[0] # cut .xlsx
    workbook = xlrd.open_workbook(filename)
    A1 = workbook.sheet_by_index(0)
    A2 = workbook.sheet_by_index(1)
    A3 = workbook.sheet_by_index(2)
    add_sheet(A1, A1_DATA_BEGIN_R, date)
    add_sheet(A2, A2_DATA_BEGIN_R, date)
    add_sheet(A3, A3_DATA_BEGIN_R, date)


for filename in glob.glob(os.path.join(path, f'*.{file_format}')):
    add_excel_file(filename)

print("OK")
