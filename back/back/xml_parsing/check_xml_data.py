"""
Project:         "Wykresator-leko-porównywator"

Description: Code we used for checking integrity of source files for our project. Program first prints if xml sheets are
             organized in the same way between files, then it prints out data that doesn't match regular expressions for
             further analysis.
"""

import re
import xlrd
from data_checker import *  # python module created by us containing function definitions we use in program

# indexes of rows (starting from 0) from which data begins in respecting sheets
A1_DATA_BEGIN_R = 3
A2_DATA_BEGIN_R = 2
A3_DATA_BEGIN_R = 2

# indexes of columns (starting from 0) in which certain data is placed
ACT_SUBS_C = 1
NAME_DOSE_C = 2
CONTENTS_C = 3
EAN_C = 4
REFUND_C = 12
PRICE_C = 15

file_path = './xml-files/01-03-2020.xlsx'
workbook = xlrd.open_workbook(file_path)
A1 = workbook.sheet_by_index(0)
A2 = workbook.sheet_by_index(1)
A3 = workbook.sheet_by_index(2)

rows_t1 = [A1_DATA_BEGIN_R - 1, A2_DATA_BEGIN_R - 1, A3_DATA_BEGIN_R - 1]
columns_t1 = [["Substancja czynna", ACT_SUBS_C], ["Nazwa  postać i dawka", NAME_DOSE_C],
              ["Zawartość opakowania", CONTENTS_C], ["Kod EAN lub inny kod odpowiadający kodowi EAN", EAN_C],
              ["Zakres wskazań objętych refundacją", REFUND_C],
              ["Wysokość dopłaty świadczeniobiorcy", PRICE_C]]

print("*** EXCEL SHEET STRUCTURE ***")
print("Excel sheet structer of type 1: ", check_worksheets_format(A1, A2, A3, rows_t1, columns_t1))


# *** CHECK ACTIVE SUBSTANCES ***
print("\n*** CHECK ACTIVE SUBSTANCES ***")
active_substances = [A1.col_values(ACT_SUBS_C, A1_DATA_BEGIN_R), A2.col_values(ACT_SUBS_C, A2_DATA_BEGIN_R),
                     A3.col_values(ACT_SUBS_C, A3_DATA_BEGIN_R)]

base_regex_subs = \
    re.compile('^([a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]+)((?! i )(?! oraz )(?! plus ) [a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]+)*\\Z')

AS1 = []
for data in active_substances:
    AS1.append(check_data(data, base_regex_subs))

print("Check 1:")
for i in AS1:
    print(i[1])

plus_regex = re.compile('[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]+((?! i )(?! oraz )(?! plus ) [+a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]+)*\\Z')

AS2 = []
for i in AS1:
    AS2.append(check_data(i[1], plus_regex))

print("Check 2:")
for i in AS2:
    print(i[1])


# *** CHECK NAME, DOSAGE AND FORM ***
print("\n*** CHECK NAME, DOSAGE AND FORM ***")
ndf = [A1.col_values(NAME_DOSE_C, A1_DATA_BEGIN_R), A2.col_values(NAME_DOSE_C, A2_DATA_BEGIN_R),
       A3.col_values(NAME_DOSE_C, A3_DATA_BEGIN_R)]

comma_regex = re.compile('.+,.+\\Z')

NDF = []
for data in ndf:
    NDF.append(check_data(data, comma_regex))

print("Check 1:")
for i in NDF:
    print(i[1])


# *** CHECK EAN ***
print("\n*** CHECK EAN ***")
eans = [A1.col_values(EAN_C, A1_DATA_BEGIN_R), A2.col_values(EAN_C, A2_DATA_BEGIN_R),
        A3.col_values(EAN_C, A3_DATA_BEGIN_R)]

base_regex_ean = re.compile('\\d{8}\\d*\\Z')

EAN = []
for data in eans:
    EAN.append(check_data(data, base_regex_ean))

print("Check 1:")
for i in EAN:
    print(i[1])


# *** CHECK PRICE ***
print("\n*** CHECK PRICE ***")
prices = [A1.col_values(PRICE_C, A1_DATA_BEGIN_R), A2.col_values(PRICE_C, A2_DATA_BEGIN_R),
          A3.col_values(PRICE_C, A3_DATA_BEGIN_R)]

base_regex_price = re.compile('\\d+,\\d{2}\\Z')

PRC = []
for data in prices:
    PRC.append(check_data(data, base_regex_price))

print("Check 1:")
for i in PRC:
    print(i[1])
