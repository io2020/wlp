"""
Project: "Wykresator-leko-porównywator"

Description: Python module used in 'check_xml_data.py'.
"""


'''
:params w1, w2, w3: excel worksheets
:param rows: List containing rows indexes from which data in file starts.
:param columns: List of pairs '[a, b]' where 'a' is expected name of column and 'b' is its index.
:return: 'True' if worksheets fulfill data in 'columns' parameter, otherwise 'False'. 
'''
def check_worksheets_format(w1, w2, w3, rows, columns):
    for column in columns:
        if (column[0] != w1.cell_value(rows[0], column[1]) or
                w1.cell_value(rows[0], column[1]) != w2.cell_value(rows[1], column[1]) or
                w2.cell_value(rows[1], column[1]) != w3.cell_value(rows[2], column[1])):
            return False

    return True


'''
:return: Tuple '(M, UM)', where 'M' is list of elements applying to regex, rest is in 'UM'. 'M' and 'UM' don't contain 
         duplicates.
'''
def check_data(data, regex):
    matched = []
    unmatched = []
    for substance in data:
        if regex.match(substance) is not None:
            if substance not in matched:
                matched.append(substance)
        else:
            if substance not in unmatched:
                unmatched.append(substance)

    return matched, unmatched
