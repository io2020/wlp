import { colors } from "@/colors";

export const rainbow = (_numOfSteps, step) => {
  return colors[step % colors.length];
};
