import React from "react";
import { SearchForm } from "@/components/SearchForm";
import "./index.css";

export const SearchPage = () => (
  <div className="search-page">
    <div className="search-box">
      <h3>Wykresator-leko-porównywator</h3>
      <SearchForm/>
    </div>
  </div>
);
