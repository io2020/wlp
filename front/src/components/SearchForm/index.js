import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import "./index.css";

export const SearchForm = () => {
  const [ean, setEan] = useState("");

  return (
    <Form
      onSubmit={(e) => {
        e.preventDefault();
        window.location.pathname = `/ean/${ean}`;
      }}
    >
      <InputGroup>
        <FormControl
          value={ean}
          onChange={(e) => setEan(e.target.value)}
          placeholder="Wyszukaj lek po EAN..."
          aria-label="Pole do wyszukiwania leku"
        />
        <InputGroup.Append>
          <a href={`/ean/${ean}`}>
            <Button className="search-btn" variant="outline-secondary">
              Szukaj
            </Button>
          </a>
        </InputGroup.Append>
      </InputGroup>
    </Form>
  );
};
