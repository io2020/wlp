import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { SearchPage } from "@/components/SearchPage"
import { MedPage } from "@/containers/MedPage"
import "./index.css";

export const App = () => (
  <Router>
    <Switch>
      <Route path="/ean/">
        <MedPage/>
      </Route>
      <Route path="/">
        <SearchPage/>
      </Route>
    </Switch>
  </Router>
);
