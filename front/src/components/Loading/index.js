import React from "react";
import Spinner from "react-bootstrap/Spinner";
import "./index.css";

export const Loading = () => (
  <div className="loading-container">
    <Spinner animation="border"/>
  </div>
);
