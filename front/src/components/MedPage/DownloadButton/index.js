import React from "react";
import Button from "react-bootstrap/Button";
import { APIHost } from "@/api/config";
import "./index.css";

export const DownloadButton = ({ ean }) => (
  <div className="med-download">
    <hr/>
    <a
      href={`${APIHost}/meds/${ean}.pdf`}
      className="med-download-button"
      rel="noopener noreferrer"
      target="_blank"
    >
      <Button variant="outline-secondary">
        Pobierz PDF
      </Button>
    </a>
  </div>
);
