import React from "react";
import { SearchForm } from "@/components/SearchForm";
import "./index.css";

export const SearchBar = () => (
  <div className="search-bar bg-light">
    <div className="search-form">
      <SearchForm/>
    </div>
  </div>
);
