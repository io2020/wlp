import React from "react";
import { Loading } from "@/components/Loading";
import { MedInfo } from "@/containers/MedPage/MedInfo";
import { DownloadButton } from "@/containers/MedPage/DownloadButton";
import { SearchBar } from "./SearchBar";
import { MedChart } from "./MedChart";
import { NotFound } from "./NotFound";
import "./index.css";

export const MedPage = ({ medFound, isInit, startedInit, doInit }) => {
  if (!startedInit) doInit();

  return <div className="med-page">
    <SearchBar/>
    <div className="med-container">
      {isInit ?
        medFound ?
          <>
            <MedInfo/>
            <MedChart/>
            <DownloadButton/>
          </>
        :
          <NotFound/>
      :
        <Loading/>}
    </div>
  </div>;
};
