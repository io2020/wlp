import React from "react";
import {
  VictoryChart,
  VictoryScatter,
  VictoryVoronoiContainer,
  VictoryLine,
  VictoryGroup,
  VictoryAxis,
} from "victory";
import "./index.css";

export const Chart = ({ dummyData, maxMedPrice, meds }) => (
  <VictoryChart
    padding={{ top: 10, bottom: 60, left: 45, right: 45 }}
    width={600}
    height={350}
    containerComponent={
      <VictoryVoronoiContainer
        mouseFollowTooltips
        voronoiBlacklist={["line"]}
        labels={({ datum: { x, y, name } }) =>
          y === null
            ? undefined
            : `${name.slice(0, 48) + (name.length > 48 ? "..." : "")} - ${x}: ${y.toFixed(2)} zł`
        }
      />
    }
  >
    <VictoryAxis
      dependentAxis
      fixLabelOverlap={true}
      domain={[0, maxMedPrice]}
      label={"Cena (zł)"}
    />
    <VictoryAxis
      fixLabelOverlap={true}
      label={"Rok i miesiąc"}
    />
    {meds.map(
      ({ id, show, color, priceByTime }) =>
        show && (
          <VictoryGroup data={priceByTime} key={id}>
            <VictoryLine
              name="line"
              style={{
                data: { stroke: color },
              }}
            />
            <VictoryScatter
              style={{
                data: { fill: color },
              }}
            />
          </VictoryGroup>
        )
    )}
    <VictoryGroup data={dummyData}>
      <VictoryLine name="line"/>
    </VictoryGroup>
  </VictoryChart>
);
