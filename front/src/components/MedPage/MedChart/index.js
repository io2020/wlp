import React from "react";
import { Chart } from "@/containers/MedPage/MedChart/Chart";
import { Picker } from "@/containers/MedPage/MedChart/Picker";
import "./index.css";

export const MedChart = () => (
  <div className="med-chart">
    <h5>
      Leki współdzielące przynajmniej jedną substancję czynną z tym lekiem
    </h5>
    <div className="chart-container">
      <Picker/>
      <Chart/>
    </div>
  </div>
);
