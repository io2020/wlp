import React from "react";
import "./index.css";

export const Picker = ({ meds, doCheck }) => (
  <div className="med-picker bg-light">
  {meds.map(({ id, ean, name, form, dose, paymentLevel, color, show }) => 
    <div key={id} className="med-option">
      <div>
        <input
          type="checkbox"
          checked={show}
          onChange={_ => doCheck(id)}
        />
      </div>
      <div>
        <a href={`/ean/${ean}`}>
          <span>{name}</span><b style={{color}}> &#9679;</b>
          <div className="info text-secondary">{form} {dose}</div>
          <div className="info text-secondary">Odpłatność: {paymentLevel}</div>
        </a>
      </div>
    </div>)}
  </div>
);
