import React from "react";
import "./index.css";

export const MedInfo = (
  { ean, name, form, dose, activeSubstances, boxContents, priceByPaymentLevel }
) => (
  <div className="med-info">
    <h1>{name}</h1>
    <br/>
    <div className="med-details">
      <div><b>EAN</b><span>{ean}</span></div>
      <div><b>Postać i dawka</b><span>{form}, {dose}</span></div>
      <div><b>Substancje czynne</b><span>{activeSubstances.join(", ")}</span></div>
      <div><b>Zawartość opakowania</b><span>{boxContents}</span></div>
    </div>
    <br/>
    <div className="med-details">
      <div><b>Poziom odpłatności</b><b>Cena</b></div>
      {priceByPaymentLevel.map(([level, price]) =>
        <div key={level}><span>{level}</span><span>{price.toFixed(2)} zł</span></div>
      )}
    </div>
  </div>
);
