import React from "react";
import "./index.css";

export const NotFound = () => (
  <div className="not-found">
    Nie znaleziono leku o takim numerze EAN.
  </div>
);
