import { connect } from "react-redux";
import { MedInfo as Component } from "@/components/MedPage/MedInfo";

const mapStateToProps = ({ medpage }) => (medpage);

export const MedInfo = connect(mapStateToProps)(Component);
