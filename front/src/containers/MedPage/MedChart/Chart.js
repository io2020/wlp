import { connect } from "react-redux";
import { Chart as Component } from "@/components/MedPage/MedChart/Chart";

const mapStateToProps = ({ medpage: { maxMedPrice, chartMeds, chartShow } }) => ({
  meds: chartMeds.map((med) => ({ ...med, show: chartShow[med.id] })),
  maxMedPrice,
  dummyData: chartMeds[0].priceByTime.map(({ x }) => ({ x, y: null }))
});

export const Chart = connect(mapStateToProps)(Component);
