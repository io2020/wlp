import { connect } from "react-redux";
import { Picker as Component } from "@/components/MedPage/MedChart/Picker";
import { checkMed } from "@/actions/medpage";

const mapStateToProps = ({ medpage: { chartMeds, chartShow } }) => ({
  meds: chartMeds.map((med) => ({...med, show: chartShow[med.id]}))
});

const mapDispatchToProps = (dispatch) => ({
  doCheck: (id) => dispatch(checkMed(id)),
});

export const Picker = connect(mapStateToProps, mapDispatchToProps)(Component);
