import { connect } from "react-redux";
import { DownloadButton as Component } from "@/components/MedPage/DownloadButton";

const mapStateToProps = ({ medpage: { ean } }) => ({ ean });

export const DownloadButton = connect(mapStateToProps)(Component);
