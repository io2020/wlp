import { connect } from "react-redux";
import { initMed } from "@/actions/medpage";
import { MedPage as Component } from "@/components/MedPage";

const mapStateToProps = ({ medpage: { ean, isInit, startedInit } }) => ({
  isInit,
  startedInit,
  medFound: isInit && ean !== null,
});

const mapDispatchToProps = (dispatch) => {
  const ean = window.location.pathname.split("/").slice(-1).pop();
  return {
    doInit: () => dispatch(initMed(ean)),
  };
};

export const MedPage = connect(mapStateToProps, mapDispatchToProps)(Component);
