import { combineReducers } from 'redux'
import { medpage } from './medpage'

export const rootReducer = combineReducers({ medpage });
