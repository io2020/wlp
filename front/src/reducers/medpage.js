import {
  CHECK_MED,
  REQUEST_MED_INIT,
  FINISH_MED_INIT,
  SET_MED_CONTENTS,
} from "@/actions/medpage";

const initialState = {
  startedInit: false,
  isInit: false,
  ean: null,
  name: null,
  activeSubstances: [],
  form: null,
  dose: null,
  boxContents: null,
  priceByPaymentLevel: [],
  maxMedPrice: 0,
  chartMeds: [],
  chartShow: {},
};

export const medpage = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_MED_INIT: {
      return {
        ...state,
        startedInit: true,
      };
    }
    case FINISH_MED_INIT: {
      return {
        ...state,
        isInit: true,
      };
    }
    case SET_MED_CONTENTS: {
      return {
        ...state,
        ...action.contents,
      };
    }
    case CHECK_MED: {
      var newState = {
        ...state,
        chartShow: { ...state.chartShow },
      };
      newState.chartShow[action.id] = !state.chartShow[action.id];
      return newState;
    }
    default:
      return state;
  }
};
