import { APIHost } from "./config.js";

const fetchAPI = (endpoint, method, data) => {
  const url = APIHost + endpoint;
  var options = {
    method: method,
    credentials: "include",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
    },
  };
  if (method === "GET") {
    options.data = data;
  } else if (method === "POST") {
    options.body = JSON.stringify(data);
  } else if (method === "DELETE") {
    options.body = JSON.stringify(data);
  } else {
    console.error(`fetchAPI: method ${method} not implemented`);
  }
  return fetch(url, options).then(
    (response) =>
      response.json().catch((e) => {
        console.log("fetch error", response, e);
        return { error: e.toString() };
      }),
    (error) => ({ error: error.toString() })
  );
};

export const fetchMed = (ean) => {
  return fetchAPI(`/meds/${ean}`, "GET", {});
};
