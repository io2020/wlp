import { fetchMed } from "@/api";
import { rainbow } from "@/utils";

export const CHECK_MED = "CHECK_MED";
export const checkMed = (id) => {
  return { type: CHECK_MED, id };
};

export const REQUEST_MED_INIT = "REQUEST_MED_INIT";
export const requestMedInit = () => {
  return { type: REQUEST_MED_INIT };
};

export const FINISH_MED_INIT = "FINISH_MED_INIT";
export const finishMedInit = () => {
  return { type: FINISH_MED_INIT };
};

export const SET_MED_CONTENTS = "SET_MED_CONTENTS";
export const setMedContents = (contents, error) => {
  return { type: SET_MED_CONTENTS, contents, error };
};

const sortChartMeds = (ean, a, b) => {
  if (a.ean === ean && b.ean === ean) {
    return 0;
  } else if (a.ean === ean) {
    return -1;
  } else if (b.ean === ean) {
    return 1;
  } else {
    const c = a.name.localeCompare(b.name);
    if (c === 0) {
      return a.paymentLevel.localeCompare(b.paymentLevel);
    }
    return c;
  }
};

export const initMed = (ean) => (dispatch) => {
  dispatch(requestMedInit());

  return fetchMed(ean).then(({ error, med, chart }) => {
    if (typeof error !== "undefined") {
      dispatch(setMedContents(null, error));
      dispatch(finishMedInit());
      return;
    }
    const {
      ean,
      name,
      active_substances,
      form,
      dose,
      box_contents,
      price_by_payment_level,
    } = med;
    var allTimes = new Set();
    for (const { price_by_time } of chart) {
      for (const time of Object.keys(price_by_time)) {
        allTimes.add(time);
      }
    }
    allTimes = Array.from(allTimes).sort((a, b) => a.localeCompare(b));
    const transformedChart = chart
      .map(({ ean, name, form, dose, payment_level, price_by_time }, i) => {
        return {
          id: i,
          ean,
          name,
          form,
          dose,
          color: rainbow(chart.length, i),
          paymentLevel: payment_level,
          priceByTime: allTimes.map((time) => ({
            x: time,
            y: price_by_time[time] ? Number(price_by_time[time]) : null,
            name,
          })),
        };
      })
      .sort((a, b) => sortChartMeds(ean, a, b));
    const contents = {
      ean,
      name,
      activeSubstances: active_substances,
      form,
      dose,
      boxContents: box_contents,
      maxMedPrice: Math.max.apply(Math, transformedChart.map(
        ({ priceByTime }) => (
          Math.max.apply(Math, priceByTime.map(({ y }) => y || 0)) || 0
        ))) || 1,
      chartMeds: transformedChart,
      chartShow: transformedChart.reduce((acc, { id }, i) => {
        acc[id] = i < 3;
        return acc;
      }, {}),
      priceByPaymentLevel: Object.entries(price_by_payment_level)
        .sort((a, b) => a[0].localeCompare(b[0]))
        .map(([time, price]) => [time, Number(price)]),
    };
    dispatch(setMedContents(contents, null));
    dispatch(finishMedInit());
  });
};
